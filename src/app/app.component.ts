import {Component, OnInit} from '@angular/core';
import {HttpService} from '../services/http/http.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'CAPCO | Angular Developer Assignment';
  subtitle = 'Jessy Riordan';
  sampleData = [];
  error: string;
  constructor( private http: HttpService) {}
  ngOnInit() {
    console.log ('testing');
    this.getSampleData();
  }
  // HTTP GET Sample Data
  getSampleData() {
    this.http.getData('../../assets/data/sample_data.json').subscribe(
      result => {
        // Handle result
        this.sampleData = result;
      },
      error => {
        // Handle error
        this.error = error;
      },
      () => {
        // 'onCompleted' callback.
        console.log('sample data: ', this.sampleData);
      }
    );
  }
  // HTTP POST Sample Data
  postSampleData(data) {
    this.http.postData(data).subscribe(
      result => {
        // Handle result
        console.log('result: ', result);
      },
      error => {
        // Handle error
        this.error = error;
      },
      () => {
        // 'onCompleted' callback.
        alert('Successfully Submitted User Details!');
      }
    );
  }
}
