import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


@Injectable()
export class HttpService {
  constructor(private http: Http) {}
  getData(url) {
    return this.http.get (url)
      .map((response: Response) => {
        return response.json();
      });
  }
  postData(data) {
    const body = JSON.stringify({id: data.id, status: data.status});
    return this.http.post ('/api/submit', body)
      .map((response: Response) => {
        return response.json();
      });
  }
}
